/**
 * @ngdoc overview
 * @name transcend.data-source
 * @description
 # Data Source Module
 The "Data Source" module contains components to create, update, delete and manage data sources. These data sources
 represent database connections.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-data-source.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-data-source/get/master.zip](http://code.tsstools.com/bower-data-source/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-data-source](http://code.tsstools.com/bower-data-source)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/data-source/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.data-source']);
 ```

 ## To Do
 - Implement unit tests.

 ## Project Goals
 - Keep this module size below 15kb minified
 */
/**
 * @ngdoc service
 * @name transcend.data-source.service:$connectionTest
 *
 * @description
 The '$connectionTest' factory provides the ability to test database connections

 * @requires transcend.core.$notify
 * @requires DataSource
 */
/**
     * @ngdoc method
     * @name transcend.data-source.service:$connectionTest#success
     * @propertyOf transcend.data-source.service:$connectionTest
     *
     * @description
     *  Tests whether or not the database connection is valid/successful.
     *
     *  @param {Object} connection object contianing connection info to test a connection.
     *  @returns {*} returns success or failure.
     */
/**
 * @ngdoc service
 * @name transcend.data-source.service:DataSource
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "DataSource" entity.
 * A "DataSource" entity models a single database source.
 *
 <pre>
 // Get a list of data sources.
 var sources = DataSource.query();

 // Get a single data source by id/name.
 var sources = DataSource.query({ id: 'MyConnection' });

 // Test a connection by id/name.
 var result = DataSource.testConnectionById({ id: 'MyConnection' });

 // Test a connection string.
 var result = DataSource.testConnection({ connectionString: '...' });
 </pre>
 *
 * @requires $resource
 * @requires transcend.transcend.data-source.dataSourceConfig
 */
/**
 * @ngdoc directive
 * @name transcend.data-source.directive:dbConnectionEditor
 *
 * @description
 * The 'dBConnectionEditor' directive provides a template for a menu to edit the database connections.
 *
 * @restrict EA
 * @scope
 *
 * @requires transcend.core.$notify
 * @requires $resourceListController
 * @requires $connectionTest
 * @requires dataSourceProviders
 *
 */
/**
 * @ngdoc directive
 * @name transcend.data-source.directive:dbConnectionList
 *
 * @description
 * The 'dbConnectionList' service
 *
 * @requires transcend-core#$notify
 * @requires transcend-resource#$resourceListController
 * @requires transcend.data-source#$connectionTest
 * @requires transcend.data-source#dataSourceProviders
 * @requires $array
 * @requires DataSource
 *
 * @param {object=} api Api object to bind to in order to gain access to directive functionality.
 * @param {boolean=} editable Determines whether the list will allow for editing data sources or not.
 * @param {function=} onSaveBegin Fired when the save process has begun.
 * @param {function=} onSave Fired when the save process has completed.
 * @param {function=} onSaveError Fired when the save process failed.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <db-connection-list controller="api"></db-connection-list>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.data-source', 'ngMockE2E'])
 .controller('Ctrl', function() {
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend){
   var mocks = [{id: 'DefaultConnection', name: 'Default Connection', provider: 'System.Data.SqlServerCe.4.0', connectionString:'Data Source=|DataDirectory|\TDS.sdf'},
              {id: 'MySqlDb', name: 'Sql Server DB', provider: 'System.Data.SqlClient', connectionString:'data source=sql.myhost.com, 1433;initial catalog=MY_CATALOG;persist security info=True;user id=suer;password=password'},
              {id: 'MyOracleDb', name: 'Oracle DB', provider: 'System.Data.OracleClient', connectionString:'Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=oracle.myhost.com)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=MY_SERVICE)));User Id=user;Password=password;'}];

   // Mock our HTTP calls.
   $httpBackend.whenGET('/api/data/source')
    .respond(angular.copy(mocks));

   angular.forEach(mocks, function(mock){
     $httpBackend.whenGET('/api/data/source/' + mock.id)
      .respond(angular.copy(mock));

     $httpBackend.whenPOST('/api/data/source/test/' + mock.id)
      .respond({"passed":true,"message":"Connection was successful","provider":mock.provider});
   });
 })
 </file>
 </example>
 */
/**
 * @ngdoc directive
 * @name transcend.controls.directive:dbConnectionNavbar
 *
 * @description
The 'dbConnectionNavbar' provides an editable navigation bar template for the database connections menu.
 *
 * @restrict EA
 * @scope
 *
 *
 */
