# Data Source Module
The "Data Source" module contains components to create, update, delete and manage data sources. These data sources
represent database connections.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-data-source.git
```

Other options:

*   * Download the code at [http://code.tsstools.com/bower-data-source/get/master.zip](http://code.tsstools.com/bower-data-source/get/master.zip)
*   * View the repository at [http://code.tsstools.com/bower-data-source](http://code.tsstools.com/bower-data-source)
*   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/data-source/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.data-source']);
```

## To Do
- Implement unit tests.

## Project Goals
- Keep this module size below 15kb minified